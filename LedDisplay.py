import time
from dotstar import Adafruit_DotStar

def LedInit():
    numpixels = 27
    datapin  = 8
    clockpin = 11

    strip = Adafruit_DotStar(numpixels, datapin, clockpin)
    strip.begin()
    strip.setBrightness(64)
    
    return(numpixel,datapin,clockpin)

def ColorChoice(couleur):
    if couleur == 'Vert' or couleur == 1 :
        
        color = 0xFF0000
        
    elif couleur == 'Rouge' or couleur == 2 :
        
        color = 0x00FF00
            
    elif couleur == 'Bleu' or couleur == 3 :
        
        color = 0x0000FF
        
    elif couleur == 'Jaune' or couleur == 4 :
        
        color = 0xFFFF00
        
    elif couleur == 'Noir' :
        color = 0x000000
    elif couleur == 'Blanc' :
        color = 0xFFFFFF
    else :
        color = couleur
    
    return(color)

def ColorShow(couleur):
    head = 0
    while(head<numpixel):
        strip.setPixelColor(head,ColorChoice(couleur))
        head += 1
    strip.show()

def ColorFlash(couleur,frequence):
    head = 0
    frequency  = frequence
    ColorShow(couleur)
    time.sleep(1.0 / frequency)
    ColorShow('Noir')
    time.sleep(1.0 / frequency)

def ColorShooting(ListeCouleur,taille):
    head = 0
    tail = -taille
	
    for color in ListeCouleur:
        loop = 1		
        while (loop == 1) :

            strip.setPixelColor(head, color)
            strip.setPixelColor(tail, 0)
            strip.show()
            time.sleep(1.0 / 50)
                
            head += 1
            if(head >= numpixels):
                head = 0
                loop = 0
                tail+=1
                if(tail >= numpixels):
                    tail = 0
