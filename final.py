import time
from RPi import GPIO
import random
import pygame.mixer
from dotstar import Adafruit_DotStar

########## Fonction initialisation capteurs ##########

##Define switch :

GPIO.setmode(GPIO.BOARD)
GPIO.setup(32,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)    
GPIO.setup(36,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(38,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)


##Define hall pull up donc pas d'aimant=1 et aimant=0

GPIO.setmode(GPIO.BOARD)
GPIO.setup(31,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(33,GPIO.IN,pull_up_down=GPIO.PUD_UP)     
GPIO.setup(35,GPIO.IN,pull_up_down=GPIO.PUD_UP)

##Define joystick :

GPIO.setmode(GPIO.BOARD)
GPIO.setup(8,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(10,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(12,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(16,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)


##Define codeur
#pas besoin de pull up, existent deja dans le composant et valent 2.7kohms, reliees au 5V.
GPIO.setmode(GPIO.BOARD)
GPIO.setup(18,GPIO.IN)   #channel A
GPIO.setup(22,GPIO.IN)   #channel I (compte le nb de tours de roue)
GPIO.setup(40,GPIO.IN)   #channel B

count=0
nb_tours=0
OPT40_B_old=0   #condition initiale de la valeur du 'passe' sur channel B


def remove():
    GPIO.remove_event_detect(31)
    GPIO.remove_event_detect(33)
    GPIO.remove_event_detect(35)
    GPIO.remove_event_detect(8)
    GPIO.remove_event_detect(10)
    GPIO.remove_event_detect(12)
    GPIO.remove_event_detect(16)
    GPIO.remove_event_detect(18)
    GPIO.remove_event_detect(22)
    GPIO.remove_event_detect(40)


########## Fin initialisation capteurs ##########

########## Debut incremental ##########

def opt(channel):
    
    global count      #decompte des paires fentes creuses/pleines       
    global nb_tours   # nombre de tours complets
    global OPT18_A
    global OPT22_I
    global OPT40_B
    global OPT40_B_old    
    
    #etats des pins
    OPT18_A=GPIO.input(18)   #pin 18, channel A
    OPT22_I=GPIO.input(22)   
    OPT40_B=GPIO.input(40)   
        
    if (OPT18_A==1 and OPT40_B_old==0) or (OPT18_A==0 and OPT40_B_old==1):
        #sens CW
        count +=1
        print ('count=', count)
        
    elif (OPT18_A==1 and OPT40_B_old==1) or (OPT18_A==0 and OPT40_B_old==0):
        #sens CCW
        count -=1
        print ('count=', count)
        
    if OPT22_I==1:
        nb_tours+=1
        count=0    #raz du decompte des fentes
        print('nb tours= ',nb_tours)
        
    #actualisation des valeurs
    OPT40_B_old=OPT40_B
    
    if (count%15==0):
        ColorShow(random.randint(1,4))



########## Fonctions LEDS ##########

numpixel = 52  #nombre de leds total
datapin  = 8   #numero du gpio (pas la pin) correspondant au data D0
clockpin = 11  #idem avec la clock C0

strip = Adafruit_DotStar(numpixel, datapin, clockpin)
strip.begin()
strip.setBrightness(64)

def ColorChoice(couleur):
    if couleur == 'Vert' or couleur == 1 : color = 0xFF0000
    elif couleur == 'Rouge' or couleur == 2 : color = 0x00FF00
    elif couleur == 'Bleu' or couleur == 3 : color = 0x0000FF
    elif couleur == 'Jaune' or couleur == 4 : color = 0xFFFF00
    elif couleur == 'Noir' : color = 0x000000
    elif couleur == 'Blanc' : color = 0xFFFFFF
    else : color = couleur
    return(color)

def ColorShow(couleur):
    head = 0
    while(head<numpixel):
        strip.setPixelColor(head,ColorChoice(couleur))
        head += 1
    strip.show()


def ColorShooting(ListeCouleur):
    head=0
    for k in range(len(ListeCouleur)):
        ColorShow(ListeCouleur[k])
        time.sleep(0.5)
        ColorShow('Noir')
        time.sleep(0.5)
                

		
		
########## Fin LEDS ##########

########## DEBUT JEU VITESSE ##########

                    
##### Il n'y a pas de limite de rapidite sur le jeu, suivant les reactions des residents une limite de temps pourra etre ajoutee. 
##### Une amelioration serait de laisser l'accompagnateur choisir le temps limite 
##### Cela pourrait se faire sous la forme d'un tableau de bord pour controler la difficulte des differents jeux : 
##### temps limite pour le jeu de rapidite + longueur du jeu, longueur de la sequence pour le jeu de memoire.
#Le compteur de fin de jeu sert a fixer une limite d'action / de temps pour le jeu. Il sera fixe a 6 
#mais une eventuelle amelioration sera de le laisser au choix des accompagnants/utilisateurs
def speed_game31(channel):
    
    speed_game(1)

def speed_game33(channel):
    speed_game(2)

def speed_game35(channel):
    speed_game(3)

def speed_gameJ(channel):
    speed_game(4)

def speed_game(var):
    global ChoixLed
    pygame.mixer.init()
   ##1=Vert 2=Rouge 3=Bleu 4=Jaune   (fonction qui fixe les leds a la couleur determinee par ChoixLed)
    if (ChoixLed == 1 and var==1) or (ChoixLed == 2 and var==2) or (ChoixLed == 3 and var==3) or (ChoixLed == 4 and var==4):
        pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/musiquereussite.ogg")
        pygame.mixer.music.play()
        ChoixLed = random.randint(1,4)
        ColorShow(ChoixLed)
    else:
        print('error')
        pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/musiqueerreur.ogg")
        pygame.mixer.music.play()
        
def interrupt_speed_game(channel):
    remove()
    global ChoixLed
    ChoixLed = random.randint(1,4)
    ColorShow(ChoixLed)
    Gpiofdetect()

def Gpiofdetect():
    GPIO.add_event_detect(8,GPIO.RISING, callback=speed_gameJ, bouncetime=200) #JAUNE
    GPIO.add_event_detect(10,GPIO.RISING, callback=speed_gameJ, bouncetime=200) #JAUNE
    GPIO.add_event_detect(12,GPIO.RISING, callback=speed_gameJ, bouncetime=200) #JAUNE
    GPIO.add_event_detect(16,GPIO.RISING, callback=speed_gameJ, bouncetime=200) #JAUNE
    GPIO.add_event_detect(31,GPIO.FALLING, callback=speed_game31, bouncetime=100) #VERT
    GPIO.add_event_detect(33,GPIO.FALLING, callback=speed_game33, bouncetime=100) #ROUGE
    GPIO.add_event_detect(35,GPIO.FALLING, callback=speed_game35, bouncetime=100) #BLEU

########## FIN JEU VITESSE ##########

########## DEBUT JEU MEMOIRE #########

def memory_gameJ(channel):
    
    pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/bidup.ogg")
    pygame.mixer.music.play()
    global Seq_joueur
    global compteur
    Seq_joueur.append(4)
    time.sleep(0.5)
    compteur+=1
    if compteur==6:
        test_seq()
    print(Seq_joueur)
    print(compteur)
    
def memory_game31(channel):
    
    pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/bidup.ogg")
    pygame.mixer.music.play()
    global Seq_joueur
    global compteur
    Seq_joueur.append(1)
    time.sleep(0.5)
    compteur+=1
    if compteur==5:
        test_seq()

def memory_game33(channel):

    pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/bidup.ogg")
    pygame.mixer.music.play()
    global compteur
    global Seq_joueur
    Seq_joueur.append(2)
    compteur+=1
    time.sleep(0.5)
    if compteur==5:
        test_seq()


def memory_game35(channel):
    pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/bidup.ogg")
    pygame.mixer.music.play()    
    global Seq_joueur
    global compteur
    Seq_joueur.append(3)
    compteur+=1
    time.sleep(0.5)
    if compteur==5:
        test_seq()


pygame.mixer.init()

def test_seq():   #verifie si la sequence est bonne
    
    global Seq_joueur
    global compteur
    if Sequence==Seq_joueur:
        
        
        pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/musiquereussite.ogg")
        pygame.mixer.music.play()
        
        time.sleep(2)
        
        interrupt_memory_game(1)
          
        
    else:
        
        pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/musiqueerreur.ogg")
        pygame.mixer.music.play()
        time.sleep(3)
        Seq_joueur=[]
        compteur=0
        ColorShooting(Sequence)
    

def interrupt_memory_game(channel2):

    remove()
    pygame.mixer.init()
    pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/electronik.ogg")
    pygame.mixer.music.play()
    
    time.sleep(3)
    global Sequence
    global Seq_joueur
    global compteur
    compteur = 0
    Seq_joueur = []
    Sequence = []    #On initialise un tableau dans lequel la sequence des Led sera enregistree
    for i in range(1,6):        #creation et sauvegarde de la sequence
        ChoixLed = random.randint(1,4)
        Sequence.append(ChoixLed)    #ex: [1,4,2,3]
    ColorShooting(Sequence)   # On affiche la sequence a memoriser
    print(Sequence)
    time.sleep(0.5)
    Gpiogdetect()
    

def Gpiogdetect():
    
    GPIO.add_event_detect(8,GPIO.RISING, callback=memory_gameJ, bouncetime=200) #JAUNE
    GPIO.add_event_detect(10,GPIO.RISING, callback=memory_gameJ, bouncetime=200) #JAUNE
    GPIO.add_event_detect(12,GPIO.RISING, callback=memory_gameJ, bouncetime=200) #JAUNE
    GPIO.add_event_detect(16,GPIO.RISING, callback=memory_gameJ, bouncetime=200) #JAUNE
    GPIO.add_event_detect(31,GPIO.FALLING, callback=memory_game31, bouncetime=200) #VERT
    GPIO.add_event_detect(33,GPIO.FALLING, callback=memory_game33, bouncetime=200) #ROUGE
    GPIO.add_event_detect(35,GPIO.FALLING, callback=memory_game35, bouncetime=200) #BLEU
    
########################################################################### FIN JEU MEMOIRE

#---------------------------------------------------------------------------------------------------------------

########################################################################### DEBUT MICRO EVENEMENTS



def micro_event(channel):
    
    ChoixDossierAudio = random.randint(0,6)
    ChoixFichierAudioOgg = random.randint(0,4)
    ColorShow(random.randint(1,4))           
    
    pygame.mixer.init()

    
    if ChoixDossierAudio == 0 :
        
        if ChoixFichierAudioOgg == 0 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/ane.ogg")       #Les musiques sont rangees dans un dossier qui peut etre facilement modifie pour proposer de
            pygame.mixer.music.play()                                                #de nouveaux sons.

                           
            
        if ChoixFichierAudioOgg == 1 :           #RAJOUTER UNE COULEUR DE LEDS ALEATOIRE POUR CHAQUE CHOIX
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/banzai.ogg")
            pygame.mixer.music.play()
 
            
            
        if ChoixFichierAudioOgg == 2 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/bidup.ogg")
            pygame.mixer.music.play() 
            
        if ChoixFichierAudioOgg == 3 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/boing.ogg")
            pygame.mixer.music.play()
            
            
        if ChoixFichierAudioOgg == 4 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/cartoon.ogg")
            pygame.mixer.music.play()

                

    if ChoixDossierAudio == 2 :
        
        if ChoixFichierAudioOgg == 0 :
            
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/chat.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 1 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/cheval.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 2 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/chien.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 3 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/coq.ogg")
            pygame.mixer.music.play()
            
        if ChoixFichierAudioOgg == 4 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/dingding.ogg")
            pygame.mixer.music.play()
                
    if ChoixDossierAudio == 3 :
    
        if ChoixFichierAudioOgg == 0 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/eau2.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 1 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/electronik.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 2 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/feu.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 3 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/grenouille.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 4 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/heat-vision.ogg")
            pygame.mixer.music.play()
             
            
    if ChoixDossierAudio == 4 :
    
        if ChoixFichierAudioOgg == 0 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/klaxoncamion.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 1 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/klaxonvoiture.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 2 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/laser.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 3 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/lion.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 4 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/loup.ogg")
            pygame.mixer.music.play()
              
            
    if ChoixDossierAudio == 5 :
    
        if ChoixFichierAudioOgg == 0 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/mouton.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 1 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/ohoh.ogg")
            pygame.mixer.music.play()
             
            
        if ChoixFichierAudioOgg == 2 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/singe.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 3 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/vache.ogg")
            pygame.mixer.music.play()
              
            
        if ChoixFichierAudioOgg == 4 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/vent.ogg")
            pygame.mixer.music.play()
              
            
    if ChoixDossierAudio == 6 :
    
        if ChoixFichierAudioOgg == 0 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/wiz.ogg")
            pygame.mixer.music.play()
            
        if ChoixFichierAudioOgg == 1 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/wiz.ogg")
            pygame.mixer.music.play()
            
        if ChoixFichierAudioOgg == 2 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/ziup.ogg")
            pygame.mixer.music.play()
            
        if ChoixFichierAudioOgg == 3 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/ziup.ogg")
            pygame.mixer.music.play()
            
        if ChoixFichierAudioOgg == 4 :
        
            pygame.mixer.music.load("/home/pi/Documents/FichierAudioOgg/electronik.ogg")
            pygame.mixer.music.play()
    
  

def interrupt_micro_event(channel):
    remove()
    print("gpiomicro")
    GPIO.add_event_detect(8,GPIO.RISING, callback=micro_event, bouncetime=200) #JAUNE
    GPIO.add_event_detect(10,GPIO.RISING, callback=micro_event, bouncetime=200) #JAUNE
    GPIO.add_event_detect(12,GPIO.RISING, callback=micro_event, bouncetime=200) #JAUNE
    GPIO.add_event_detect(16,GPIO.RISING, callback=micro_event, bouncetime=200) #JAUNE
    GPIO.add_event_detect(31,GPIO.FALLING, callback=micro_event, bouncetime=200)   #VERT
    GPIO.add_event_detect(33,GPIO.FALLING, callback=micro_event, bouncetime=200)   #ROUGE
    GPIO.add_event_detect(35,GPIO.FALLING, callback=micro_event, bouncetime=200)   #BLEU
    GPIO.add_event_detect(18,GPIO.RISING, callback=opt, bouncetime=1)   #OPTIQUE CHANNEL A
    
########################################################################### FIN MICRO EVENEMENTS


### Choix du mode :
GPIO.add_event_detect(32,GPIO.BOTH, callback=interrupt_speed_game, bouncetime=3000)    #vitesse

GPIO.add_event_detect(36,GPIO.BOTH, callback=interrupt_memory_game, bouncetime=3000)    #memoire

GPIO.add_event_detect(38,GPIO.BOTH, callback=interrupt_micro_event, bouncetime=3000)    #micro evmts



